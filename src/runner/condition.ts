/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Str,
    condition,
    isVariable,
    tripetto,
} from "@tripetto/runner";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class EmailCondition extends ConditionBlock<{
    readonly mode: TMode;
    readonly match?: string;
}> {
    @condition
    isMatch(): boolean {
        const emailSlot = this.valueOf<string>();

        if (emailSlot) {
            const value = Str.lowercase(emailSlot.string);
            const match = Str.lowercase(
                (() => {
                    if (isVariable(this.props.match)) {
                        const variable = this.variableFor(this.props.match);

                        return variable && variable.hasValue
                            ? variable.string
                            : "";
                    }

                    return this.parseVariables(this.props.match || "");
                })()
            );

            switch (this.props.mode) {
                case "domain":
                case "not-domain":
                    return (
                        (match &&
                            (value.substr(value.lastIndexOf("@") + 1) ===
                                match) ===
                                (this.props.mode === "domain"
                                    ? true
                                    : false)) ||
                        false
                    );
                case "address":
                case "not-address":
                    return (
                        (value === match) ===
                            (this.props.mode === "address" ? true : false) ||
                        false
                    );
                case "defined":
                    return value !== "";
                case "undefined":
                    return value === "";
            }
        }

        return false;
    }
}
